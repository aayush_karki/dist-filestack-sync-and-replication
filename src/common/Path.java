package common;

import java.io.*;
import java.util.*;




/** Distributed filesystem paths.

    <p>
    Objects of type <code>Path</code> are used by all filesystem interfaces.
    Path objects are immutable.

    <p>
    The string representation of paths is a forward-slash-delimeted sequence of
    path components. The root directory is represented as a single forward
    slash.

    <p>
    The colon (<code>:</code>) and forward slash (<code>/</code>) characters are
    not permitted within path components. The forward slash is the delimeter,
    and the colon is reserved as a delimeter for application use.
 */
public class Path implements Iterable<String>, Comparable<Path>, Serializable
{
    private ArrayList<String> pathArrayList = null; 
    

    /** Creates a new path which represents the root directory. */
    public Path()
    {
        // an initialized both non-null and empty pathArrayList represents the root.
        this.pathArrayList =  new ArrayList<String>();
    }

    /** Creates a new path by appending the given component to an existing path.

        @param path The existing path.
        @param component The new component.
        @throws IllegalArgumentException If <code>component</code> includes the
                                         separator, a colon, or
                                         <code>component</code> is the empty
                                         string.
    */
    public Path(Path path, String component)
    {
    	this();

        if (component==null || path==null){
        	throw new IllegalArgumentException();
        }
        
        //check compenent for illegality
        if (    component.equals("")
        		||component.contains("/")
                || component.contains(":")
                || component.isEmpty() ) {
            throw new IllegalArgumentException(); 
            
        }
        Iterator<String> path_iter = path.iterator();

        while (path_iter.hasNext()){
            String comp = path_iter.next();
            this.pathArrayList.add(comp);
        }

        this.pathArrayList.add(component);

    }

    /** Creates a new path from a path string.

        <p>
        The string is a sequence of components delimited with forward slashes.
        Empty components are dropped. The string must begin with a forward
        slash.

        @param path The path string.
        @throws IllegalArgumentException If the path string does not begin with
                                         a forward slash, or if the path
                                         contains a colon character.
     */
    public Path(String path)
    {
    	this();
        if (path==null){
            throw new IllegalArgumentException();
        }

        if ((path.length()==0)
        	||path.contains(":")        	
            || (!(path.charAt(0)=='/'))) {
        	 
            throw new IllegalArgumentException("something wrong here! path ="+ path);
        }


        
        String[] components = path.split("/");

        for (String word: components) {
               //drop empty component
            if (!word.isEmpty()){
                this.pathArrayList.add(word);
            }
        }

        


    }

    /** Returns an iterator over the components of the path.

        <p>
        The iterator cannot be used to modify the path object - the
        <code>remove</code> method is not supported.

        @return The iterator.
     */
    @Override
    public Iterator<String> iterator()
    {
        return new Iterator<String>(){
            private int index = 0;

            public boolean hasNext(){
                return index < pathArrayList.size(); 
            } //hasNext()

            public String next(){
                if (!hasNext()){
                    throw new NoSuchElementException();
                }
                return pathArrayList.get(index++);
            }//next()

            public void remove(){
                throw new UnsupportedOperationException(
                    "Remove method not supported by iterator");
            }

        }; 
    }

    /** Lists the paths of all files in a directory tree on the local
        filesystem.

        @param directory The root directory of the directory tree.
        @return An array of relative paths, one for each file in the directory
                tree.
        @throws FileNotFoundException If the root directory does not exist.
        @throws IllegalArgumentException If <code>directory</code> exists but
                                         does not refer to a directory.
     */
    public  static Path[] list(File directory) throws FileNotFoundException
    {

        if (!directory.exists()) throw new FileNotFoundException();
        if (!directory.isDirectory()) throw new IllegalArgumentException();

		try {
			ArrayList<Path> buffer = new ArrayList<Path>();

			int start_idx = directory.getCanonicalPath().length() ;
			fillArrayList_withPaths(directory, buffer, start_idx);
	        
	        return buffer.toArray(new Path[]{});
	    
		} catch (IOException e) {
				e.printStackTrace();
		}
        
       return null;

    }

    
    
    private static void fillArrayList_withPaths(File f , ArrayList<Path> buffer, int start) 
    throws IOException{
        


        if (f.isFile()){
        	/* the default file separator in OS like Windows is different than
        	 * '/' . So we need to replace '\' with '/' .
        	 * */
        	String fullpath = f.getCanonicalPath();
        	String fullpath_adjusted = fullpath.replace(File.separatorChar, '/');
        	
        	String relativePath = fullpath_adjusted.substring(start);  	
        	Path newPath = new Path(relativePath);
        	buffer.add(newPath);
        }

        File[] fileOrDirs = f.listFiles();
        if (fileOrDirs==null || fileOrDirs.length==0)
            return ;

        for (File fd: fileOrDirs){
        	fillArrayList_withPaths(fd, buffer, start);
        }

        return;

    }

    /** Determines whether the path represents the root directory.

        @return <code>true</code> if the path does represent the root directory,
                and <code>false</code> if it does not.
     */
    public boolean isRoot()
    {
        if (this.pathArrayList==null) return false;

        return (this.pathArrayList.size()==0);
    }

    /** Returns the path to the parent of this path.

        @throws IllegalArgumentException If the path represents the root
                                         directory, and therefore has no parent.
     */
    public Path parent()
    {
        if (isRoot()) throw new IllegalArgumentException();

        int lastIndex = this.pathArrayList.size() - 1;
        String component = last();
        String parentStr ;

        synchronized(this){
            this.pathArrayList.remove(lastIndex);
            parentStr = this.toString();
            this.pathArrayList.add(new String(component));
        }

        return new Path(new String(parentStr));

    }

    /** Returns the last component in the path.

        @throws IllegalArgumentException If the path represents the root
                                         directory, and therefore has no last
                                         component.
     */
    public String last()
    {
        if (isRoot()) 
            throw new IllegalArgumentException("Root has not component");

        int lastIndex = this.pathArrayList.size() - 1;
        return new String(this.pathArrayList.get(lastIndex));
    }

    /** Determines if the given path is a subpath of this path.

        <p>
        The other path is a subpath of this path if is a prefix of this path.
        Note that by this definition, each path is a subpath of itself.

        @param other The path to be tested.
        @return <code>true</code> If and only if the other path is a subpath of
                this path.
     */
    public boolean isSubpath(Path other)
    {
        String otherPath = other.toString();
        String ourPath = this.toString();

        return ourPath.startsWith(otherPath);
    }

    /** Converts the path to <code>File</code> object.

        @param root The resulting <code>File</code> object is created relative
                    to this directory.
        @return The <code>File</code> object.
     */
    public File toFile(File root)
    {
        File res = null;
        String relativePath = toString();
        try{
            String canonPathString = root.getCanonicalPath() + relativePath;
            res = new File(canonPathString);
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }       
        return res;
        
    }

    /** Compares two paths for equality.

        <p>
        Two paths are equal if they share all the same components.

        @param other The other path.
        @return <code>true</code> if and only if the two paths are equal.
     */
    @Override
    public boolean equals(Object other)
    {
        if ( !(other instanceof Path)) return false;

        Path otherPath = (Path) other;
        
        String ourPathString = this.toString();
        String otherPathStrng = otherPath.toString();

        return ourPathString.equals(otherPathStrng);
    }

    /** Returns the hash code of the path. */
    @Override
    public int hashCode()
    {
        return this.pathArrayList.hashCode()*17 + 15440;
    }

    /** Converts the path to a string.

        <p>
        The string may later be used as an argument to the
        <code>Path(String)</code> constructor.

        @return The string representation of the path.
     */
    @Override
    public String toString()
    {

        int size = this.pathArrayList.size();
        if (size==0) return "/";

        String path = "";
        for (int i=0; i<size; i++ ) {
            path += "/" + this.pathArrayList.get(i);
        }

        return path;
    }

    
	@Override
	public int compareTo(Path otherPath) {
		// TODO Auto-generated method stub
		if(this.equals(otherPath)){
			return 0;
		}
		else if(isSubpath(otherPath)){
			return 1;
		}
		else{
			return -1;
		}
	}


}
