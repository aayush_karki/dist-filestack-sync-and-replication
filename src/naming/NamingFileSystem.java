package naming;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import common.Path;
import rmi.RMIException;
import storage.Command;
import storage.Storage;

public class NamingFileSystem {
	public HashMap<Path, Storage_Command> NS_Files; //NamingServer fileSystem files
	public HashSet <Path> NS_Directories;		//NamingServer fileSystem directories
	
	public HashMap<Storage_Command, HashSet<Path>> storageServers;
	
	private int nextStorageServer_idx = 0;
	
    private class Storage_Command{
    	private Storage storage=null;
    	private Command command=null;
    	
		public Storage_Command(Storage storage, Command command) {
			this.storage = storage;
			this.command = command;
		}

		public Storage getStorage() {
			return storage;
		}

		public Command getCommand() {
			return command;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((command == null) ? 0 : command.hashCode());
			result = prime * result + ((storage == null) ? 0 : storage.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Storage_Command other = (Storage_Command) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (command == null) {
				if (other.command != null)
					return false;
			} else if (!command.equals(other.command))
				return false;
			if (storage == null) {
				if (other.storage != null)
					return false;
			} else if (!storage.equals(other.storage))
				return false;
			return true;
		}

		private NamingFileSystem getOuterType() {
			return NamingFileSystem.this;
		}

		
    }

	
	public NamingFileSystem(){
        NS_Files= new HashMap<Path, Storage_Command>();
        NS_Directories = new HashSet<Path>();
        NS_Directories.add(new Path());// the FS of the NS should always have a root.
        storageServers = new HashMap<Storage_Command, HashSet<Path>>();
	}
	
	
	public boolean hasPath(Path path){
		return NS_Directories.contains(path);
	}
	
	public Set<Path> getAllFiles(){
		 return  NS_Files.keySet();
	}
	
	public HashSet<Path> getAllDirectories(){
		return NS_Directories;
	}
	
    /*
     * Returns a storage and command tuple server. 
     * Conceptualizes the list of servers being stored in a circular chain.
     * Every server is returned once in a cycle.
     * */
    public Storage_Command getStorageServer ()
    {
   	
        int idx = nextStorageServer_idx;
        int count = storageServers.size();
        
        if (nextStorageServer_idx < count-1){
        	nextStorageServer_idx++;
        }
        else{
        	nextStorageServer_idx = (nextStorageServer_idx+1)%count;
        }
     
        Storage_Command serverCmd = (Storage_Command)storageServers.keySet().toArray()[idx];
        
//        Object[] tuple = new Object[]{serverCmd.getStorage(), 
//        							 serverCmd.getCommand()};
        return serverCmd;
       
    }
	
	public boolean create(Path file) throws RMIException{
		
		if(exists(file)) return false;
				
        Storage_Command tuple = getStorageServer();
        if(tuple.getCommand().create(file)){
        	buildAllSubdirectories(file);
        	NS_Files.put(file,tuple);
        	storageServers.get(tuple).add(file);
        	return true;
        }
        else{
        	return false;
        }
	}
	
    /*
     * @param path - path is non-null and is not a root.
     * */   
    private void buildAllSubdirectories(Path path){
    	Path parent = path.parent();
    	while (!parent.isRoot()){
    		NS_Directories.add(parent);
    		parent = parent.parent();
    	}
    }
    
    public boolean addDirectory(Path directory){
    	return NS_Directories.add(directory);
    }

    public boolean removeFile(Path file) throws RMIException{
    	//delete path from the storage before from Naming as deleting might raise RMI Exp
    	//so we want to avoid path being deleted from NS but not from the storage
    	Storage_Command server = NS_Files.get(file);
    	server.getCommand().delete(file);
    	
    	return (NS_Files.remove(file)!=null) 
    			&& 
    			storageServers.get(server).remove(file);
    }
    
    public boolean removeDirectory(Path dir){
    	return NS_Directories.remove(dir);
    }
    
    public Storage getStorage(Path file) throws FileNotFoundException{
    	Storage_Command server_tuple = NS_Files.get(file);
    	if (server_tuple ==null) throw new FileNotFoundException(
    			"Path file not found in the naming server FS"
    			);
    	return server_tuple.getStorage();
    }
    
    public ArrayList<Path> register(Storage client_stub, Command command_stub, Path[] files){
    	
    	Storage_Command tuple = new Storage_Command(client_stub, command_stub);
		if(storageServers.keySet().contains(tuple) ){
					throw new IllegalStateException("the storage server"
					+ "has already been registered. Can't re-register");
		}
		
		storageServers.put(tuple, new HashSet<Path>());
		ArrayList<Path> duplicatePaths = new ArrayList<Path>();
		
		// @param 'files' contains all files, no directories.
		for(Path path: files){
			if(path.isRoot()){
				;//do nothing
			}
			else if(exists(path)){
				duplicatePaths.add(path);
			}
			else{
				NS_Files.put(path, tuple);
				buildAllSubdirectories(path);
				storageServers.get(tuple).add(path);
			}
		}
		return duplicatePaths;
    }
    
    /*
     * Return true if and only if path is a file or a directory
     * that is already registered in this NamingServer
     * */    
    public boolean exists(Path path){
    	if (NS_Directories.contains(path)) return true;
    	if (NS_Files.containsKey(path)) return true;
    	
    	return false;
    }
}//class
