package naming;


import java.io.*;

import rmi.*;
import common.*;
import storage.*;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashSet;

/** Naming server.

    <p>
    Each instance of the filesystem is centered on a single naming server. The
    naming server maintains the filesystem directory tree. It does not store any
    file data - this is done by separate storage servers. The primary purpose of
    the naming server is to map each file name (path) to the storage server
    which hosts the file's contents.

    <p>
    The naming server provides two interfaces, <code>Service</code> and
    <code>Registration</code>, which are accessible through RMI. Storage servers
    use the <code>Registration</code> interface to inform the naming server of
    their existence. Clients use the <code>Service</code> interface to perform
    most filesystem operations. The documentation accompanying these interfaces
    provides details on the methods supported.

    <p>
    Stubs for accessing the naming server must typically be created by directly
    specifying the remote network address. To make this possible, the client and
    registration interfaces are available at well-known ports defined in
    <code>NamingStubs</code>.
 */
public class NamingServer implements Service, Registration 
{
	private Skeleton<Service> skeleton_serv;
	private Skeleton<Registration> skeleton_reg;
	
	private NamingFileSystem root;

		
	private int STATUS;
	/*There are three states
	 * -the server is up and ready to receive registrations
	 * -the server has registered at least one storage server and is doing
	 *  its Naming tasks
	 *  -the server has been stopped explicitly, and it won't restart after this*/
	private static final int READY = 0;
	private static final int ACTIVE = 1;
	private static final int STOPPED = 2;
	
	
    /** Creates the naming server object.

        <p>
        The naming server is not started.
     */
    public NamingServer()
    {
    	 
        skeleton_serv = new Skeleton<Service>(Service.class, this,
        		new InetSocketAddress(NamingStubs.SERVICE_PORT));
        
        skeleton_reg = new Skeleton<Registration>(Registration.class, this,
        		new InetSocketAddress(NamingStubs.REGISTRATION_PORT));
        

        this.root = new NamingFileSystem();
        STATUS = READY;
        
    }

    /** Starts the naming server.

        <p>
        After this method is called, it is possible to access the client and
        registration interfaces of the naming server remotely.

        @throws RMIException If either of the two skeletons, for the client or
                             registration server interfaces, could not be
                             started. The user should not attempt to start the
                             server again if an exception occurs.
     */
    public synchronized void start() throws RMIException
    {
        if (STATUS == ACTIVE){
        	return ;
        }
        else if (STATUS == READY){
        	try {
				skeleton_serv.start();
				skeleton_reg.start();
				STATUS = ACTIVE;
			} catch (RMIException e) {
				throw e;
			}
        	catch(Exception e){
        		e.printStackTrace();;
        	}
        }
        else if(STATUS == STOPPED){
        	throw new  UnsupportedOperationException("Attempting" 
        			+" to start a stopped Naming Server"
        			+ "This is not allowed");
        	
        }
        else{
        	throw new Error("Invalid value of STATUS detected.");
        }
    }
    
 

    /** Stops the naming server.

        <p>
        This method waits for both the client and registration interface
        skeletons to stop. It attempts to interrupt as many of the threads that
        are executing naming server code as possible. After this method is
        called, the naming server is no longer accessible remotely. The naming
        server should not be restarted.
     */
    public void stop()
    {
        if(STATUS==READY || STATUS==STOPPED) return;

			skeleton_serv.stop();
			skeleton_reg.stop();
			stopped(null);			

    }

    /** Indicates that the server has completely shut down.

        <p>
        This method should be overridden for error reporting and application
        exit purposes. The default implementation does nothing.

        @param cause The cause for the shutdown, or <code>null</code> if the
                     shutdown was by explicit user request.
     */
    protected void stopped(Throwable cause)
    {
    		STATUS = STOPPED;
    		
    }

    // The following methods are documented in Service.java.
    @Override
    public boolean isDirectory(Path path) throws FileNotFoundException
    {
    	
    	if (path==null) throw new NullPointerException("Path is null");
    	if (!root.exists(path)) throw new FileNotFoundException("path doesnt exist"
    			+ "in naming server's file system");
    	
    	return root.hasPath(path);

    }
    
    @Override
    public String[] list(Path directory) throws FileNotFoundException
    {
           	 
    	if (directory==null) throw new NullPointerException("Given dir is null");
    	
    	if (!isDirectory(directory)) throw new FileNotFoundException("Given dir is not in FS");
    	
    	ArrayList<String> children = new ArrayList<String>();
    	
    	String dirStr = directory.toString();
    	
     	//TODO fix list!
    	for (Path file: root.getAllFiles()){
    		if(!file.isRoot()){
    			if(dirStr.equals(file.parent().toString())){
    				children.add(file.last());
    			}
    		}
    	}
    	
    	
    	for (Path dir: root.getAllDirectories()){
    		if(!dir.isRoot()){
    			String parent = dir.parent().toString();
    			if (dirStr.equals(parent)){
    				children.add(dir.last());
    			}
    		}
    	}//for
    	

    	String[] children_str = new String[children.size()];
    	for(int i=0; i<children.size(); i++){
    		children_str[i] = children.get(i);
    	}
    	
    	return children_str;
    	
    }
    
   

    @Override
    public boolean createFile(Path file)
        throws RMIException, FileNotFoundException
    {
        if (file==null) throw new NullPointerException("Null path given.");
        
        if(!file.isRoot() && !root.hasPath(file.parent())){
        	throw new FileNotFoundException("Parent dir of the file"
        			+ "doesn't exist");
        }
        
//        if(exists(file)) return false;
        
//        Object[] strgCmdPair =(Object[]) root.getStorageServer();
//        Object strg = (Storage)strgCmdPair[0];
//        Command cmd =(Command)strgCmdPair[1];
        
//        Storage_Command tuple = getStorageServer();
//        if(cmd.create(file)){
//        	buildAllSubdirectories(file);
//        	NS_Files.put(file,tuple);
//        	storageServers.get(tuple).add(file);
//        	return true;
//        }
//        else{
//        	return false;
//        }
        
        return root.create(file);
    }
   
    
    @Override
    public boolean createDirectory(Path directory) throws FileNotFoundException
    {
        if (directory==null) throw new NullPointerException("Null directory given.");
        
        if(directory.isRoot() || root.exists(directory)){
        	return false;
        }
        
        if(!isDirectory(directory.parent())){
        	throw new FileNotFoundException("the parent of the given dir"
        			+ "doesn't exist in the Naming FS");
        }
        
        return root.addDirectory(directory);
    }

    @Override//i added RMIException
    public boolean delete(Path path) throws FileNotFoundException, RMIException
    {
        if (path==null) throw new NullPointerException("Given path is null");
        
        if (path.isRoot()) return false;
        
        if (!root.exists(path)) throw new FileNotFoundException("path doesnt exist in the file system");
                
       
        if (!isDirectory(path)){
        	//delete path from the storage before from Naming as deleting might raise RMI Exp
        	//so we want to avoid path being deleted from NS but not from the storage
        	return root.removeFile(path);
        }
        
        /*remove an empty dir*/
        if (list(path).length == 0){
        	//TODO something might be needed to be added here. What if there is an empty dir in storage server??
        	//Can there even ever be an empty directory in a storage server since the storage server prunes when
        	//we issue command to it to delete a file?
        	//We dont have Storage_Command tuple for a directory. we only have for files. 
        	//Bottomline: does the problem i'm thinking of even exists?
        	
        	return root.removeDirectory(path);
        }
        /*remove a non-empty dir*/
        else{
        	//all the files in the directories have to be deleted before deleting the subdirectories i think.
        	
        	for (Path f : root.getAllFiles()) {
                if (f.parent ().equals (path)) {
                    delete(f);
                }
        	} 
        	
        	for (Path f : root.getAllDirectories()  ){
        		if (f.parent().equals(path)){
        			delete(f);
        		}
        	}
    	
        }
        return true;
        
    }

    @Override
    public Storage getStorage(Path file) throws FileNotFoundException
    {
    	if (file ==null) throw new NullPointerException("Given file is null");
    	
//    	Storage_Command server_tuple = NS_Files.get(file);
//    	if (server_tuple ==null) throw new FileNotFoundException(
//    			"Path file not found in the naming server FS"
//    			);
//    	return server_tuple.getStorage();
    	return root.getStorage(file);
    }

    // The method register is documented in Registration.java.
    @Override
    public Path[] register(Storage client_stub, Command command_stub,
                           Path[] files)
    {
    	
    	if (client_stub==null || command_stub==null ||files==null)
    		throw new NullPointerException(
    				"client_stub or command_stub or files is null");

//    	Storage_Command tuple = new Storage_Command(client_stub,
//    												command_stub);
//    	if(storageServers.keySet().contains(tuple) ){
//    		throw new IllegalStateException("the storage server"
//    				+ "has already been registered. Can't re-register");
//    	}
//        
//    	storageServers.put(tuple, new HashSet<Path>());
//    	ArrayList<Path> duplicatePaths = new ArrayList<Path>();
    	
    	// @param 'files' contains all files, no directories.
    	
//    	for(Path path: files){
//    		if(path.isRoot()){
//    			;//do nothing
//    		}
//    		else if(exists(path)){
//    			duplicatePaths.add(path);
//    		}
//    		else{
//    			NS_Files.put(path, tuple);
//    			buildAllSubdirectories(path);
//    			storageServers.get(tuple).add(path);
//    		}
//    	}
    	
    	ArrayList<Path> duplicatePaths = root.register(client_stub, command_stub, files);
    	Path[] duplicates = new Path[duplicatePaths.size()];
    	for (int i=0; i<duplicatePaths.size(); i++){
    		duplicates[i] = duplicatePaths.get(i);
    	}
    	
    	return duplicates;
    }
    

    

    
//*********************************************************************

    

    

	@Override
	public void lock(Path path, boolean exclusive) throws RMIException, FileNotFoundException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unlock(Path path, boolean exclusive) throws RMIException {
		// TODO Auto-generated method stub
		
	}
    

}