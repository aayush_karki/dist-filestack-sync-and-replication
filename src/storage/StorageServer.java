package storage;

import java.io.*;
import java.net.*;
import java.util.*;

import common.*;
import rmi.*;
import naming.*;

/** Storage server.

    <p>
    Storage servers respond to client file access requests. The files accessible
    through a storage server are those accessible under a given directory of the
    local filesystem.
 */
public class StorageServer implements Storage, Command {

	private Skeleton<Command> skeleton_cmd;
	private Skeleton<Storage> skeleton_strg;    
    private File root;

   
    /** Creates a storage server, given a directory on the local filesystem.

    @param root Directory on the local filesystem. The contents of this
                directory will be accessible through the storage server.
    @throws NullPointerException If <code>root</code> is <code>null</code>.
     */
    public StorageServer(File root) {
    	if (root==null) throw new NullPointerException("root cannot be null");
        this.root = root;
        skeleton_cmd = new Skeleton<Command>(Command.class, this);
        skeleton_strg = new Skeleton<Storage>(Storage.class, this);
 
    }

    /** Starts the storage server and registers it with the given naming
    server.

    @param hostname The externally-routable hostname of the local host on
                    which the storage server is running. This is used to
                    ensure that the stub which is provided to the naming
                    server by the <code>start</code> method carries the
                    externally visible hostname or address of this storage
                    server.
    @param naming_server Remote interface for the naming server with which
                         the storage server is to register.
    @throws UnknownHostException If a stub cannot be created for the storage
                                 server because a valid address has not been
                                 assigned.
    @throws FileNotFoundException If the directory with which the server was
                                  created does not exist or is in fact a
                                  file.
    @throws RMIException If the storage server cannot be started, or if it
                         cannot be registered.
 */
    public synchronized void start(String hostname, Registration naming_server)
            throws RMIException, UnknownHostException, FileNotFoundException
        {
           skeleton_cmd.start();
           skeleton_strg.start();
       
           Path[] paths = naming_server.register(
    				    		   Stub.create(Storage.class, skeleton_strg), 
    				    		   Stub.create(Command.class, skeleton_cmd) ,
    				    		   Path.list(root)
        	       		   );
           for(Path path: paths)//delete duplicate paths
        	   delete(path);
        }



    /** Stops the storage server.

        <p>
        The server should not be restarted.
     */
    public void stop()
    {
        try{
        	skeleton_cmd.stop();
        	skeleton_strg.start();
        	stopped(null);
        }
        catch(Exception e){
        	//skip
        }
    }


    /** Called when the storage server has shut down.

        @param cause The cause for the shutdown, if any, or <code>null</code> if
                     the server was shut down by the user's request.
     */
    protected void stopped(Throwable cause)
    {
    	//do something later
    	return;
    }
    
    

    // The following methods are documented in Storage.java.
    @Override
    public synchronized long size(Path file) throws FileNotFoundException
    {
        File f = convertIntoFile(file);
        
        if(!f.exists())
        	throw new FileNotFoundException("the file/dir doesn't exist.");
        
        if (f.isFile()) 
        	return (long)f.length();
        else{
        	throw new FileNotFoundException("File expected. Direcotry given");
        }
        
		 
    }
    
    

    @Override
    public synchronized byte[] read(Path file, long offset, int length)
        throws FileNotFoundException, IOException
    {
        File f = convertIntoFile(file);
        
        if(!f.exists() || f.isDirectory())
        	throw new FileNotFoundException();

		if (length<0 || offset<0)
			throw new IndexOutOfBoundsException("offset and length must be +ve");
		
        byte[] buffer = new byte[length];
        if (length==0)  return buffer;
        
        if(length + offset >f.length()){
        	throw new IndexOutOfBoundsException("lenght+offset sud be less than"
        			+ "file's length");
        }
        

        RandomAccessFile in = null;
		try {
			in = new RandomAccessFile(f, "r");
			in.read(buffer, (int)offset, length);

		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("invalid file.");
		}
		 catch(Exception e){
			 e.printStackTrace();
		 }
        finally{
        	if(in !=null) in.close();
        }
           
        return buffer;
    }
    
    
    

    @Override
    public synchronized void write(Path file, long offset, byte[] data)
        throws FileNotFoundException, IOException
    {
        if (offset<0) throw new IndexOutOfBoundsException("offset sud be >0");
        if (file ==null || data==null) 
        	throw new NullPointerException("file or data is null");
        
        if (data.length==0) return;
        
        RandomAccessFile out = null;
        File f = convertIntoFile(file);
        if (!f.exists() || f.isDirectory())
        	throw new FileNotFoundException();
        try{
        	out = new RandomAccessFile(f, "rws");
        	out.seek(offset);
        	out.write(data, (int)0, (int) data.length);
        }
        catch(FileNotFoundException e){
        	throw e;
        }
        finally{
        	if (out!=null) out.close();
        }
        return;
    }
    
    

    // The following methods are documented in Command.java.
    @Override
    public synchronized boolean create(Path file)
    {
        if (file==null){
        	throw new NullPointerException("path file cannot be null");
        }
        
        if (file.isRoot()) 
        	return false;
        
        File newFile = convertIntoFile(file);
        if (newFile.exists()) return false;
        File parentDir = convertIntoFile(file.parent());
        
        try {
			parentDir.mkdirs();
			newFile.createNewFile();
			return true;
		} catch (IOException e) {			
			return false;
		}
               
    } 


    @Override
    public synchronized boolean delete(Path path)
    {
    	if (path.isRoot()) return false;
    	
    	File file = convertIntoFile(path);
    	
    	if (!file.exists()) return false;
    	
    	
    	if (rmDir(file)) {
    		prune(file.getParentFile());
    		return true;
    	}
    	else{
    		return false;
    	}
    	
    }

    
    /***********************************************/
    // 			Helper function added by me!
    /***********************************************/
    
    
    
    /** This is a helper function. It recursively deletes empty 
     * folers including those that contain other empty folders.
     * @param file
     */
    private void prune(File file){
    	if(file.equals(root))
    		return;
    	
    	if(file.list().length==0){
    		File parent = file.getParentFile();
    		file.delete();
    		prune(parent);
    	}
    	return;
    }
    

    /**
     * @param dir - should be a directory
     * @return - if all subdirectories removed then return true
     * 			 else false
     */
    private boolean rmDir(File dir){
    	
    	if(dir.isFile()){
    		return dir.delete();
    	}
    	else{
    		File[] children = dir.listFiles();
    		for (File f: children){
    			if(!rmDir(f)) return false;
    		}
    		return dir.delete();
    		
    	}
    }
     

    /** Takes a Path and converts into type File
     * @param path - Should be of type directory
     * @return Corresponding File value
     */
    private File convertIntoFile(Path path){
		
		String rootPath = null;
		try {
			rootPath = root.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		rootPath += path.toString().replace("/", File.separator);
		return new File(rootPath);
    	
    }

	@Override
	public boolean copy(Path filePath, Storage server) 
			throws RMIException, FileNotFoundException, IOException 
	{
		if (filePath==null || server==null)
			throw new NullPointerException("Null arguments given.");
		
		File file = filePath.toFile(root);
		if (file == null) throw new FileNotFoundException(" @file doesn't exist in the storage server");
		
		if(file.exists() ){
			delete(filePath);
		}
		
		
		//return true if it replicates @filePath in the storage server
		if(create(filePath)){ //if filePath is duplicated in the storage server
			long filesize = server.size(filePath);
			long offset = 0;
			while(offset<filesize){
				int copySize = (int) Math.min(Integer.MAX_VALUE, filesize-offset);
				byte[] buffer = server.read(filePath, offset, copySize);
				write(filePath, offset, buffer);
				offset += copySize;
			}
			return true;
		}else{
			return false;
		}

	}    
}



















